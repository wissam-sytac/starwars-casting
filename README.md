# StarWars Casting
Live url: https://wissam-sytac.gitlab.io/starwars-casting/
(Gitlab pages)

## Budget:
Total time spent: 7 hours.

## General Notes:
The initial budget 4-5 hours is insufficient to carry out this task and build this app in the best way possible.

Especially when you factor in things like initial project setup and backend technical limitations (as explained below).

We have taken shortcuts in terms of UI and styling in order to achieve the best possible trade-off in terms of product quality.

## Technical Considerations

### 1. Backend API Limitation for filtering

The StarWars API (https://swapi.dev/) **does NOT provide a way to search characters by film**.
In a normal project setting, a Frontend engineer would want to bring this up as a limitation on the API.

**In other words, without backend API support, implementing this filtering feature in the frontend is NOT recommended.**

However, for the purpose of this exercise, I have implemented the search functionality on the frontend side as a challenge.
  
### 2. Frontend Filtering is sub-optimal

With the current implementation, when the use filters by a film, the app performs a getFilmById request
for each of the selected movies. Then, it calculates the intersection of the sets of characters common
among those films. After that, a getCharacterById is sent for each of the characters. As a result, we
may be issuing a lot of requests which is not ideal in many ways.

Additionally, pagination is disabled for filtering results as a consequence of the above mentioned 
considerations.

### 3. Caching

We utilize the caching mechanism that comes out-of-the-box with Redux toolkit. Results of outbound requests
cached for a limited period of time.

### 4. Test Coverage

The codebase comes with a small set of tests. Can be expanded if more time available.

Additionally, things like linting and automatic formatting are not part of this version.

### 5. Error Handling

Error displaying could use improvement. Again, was unable to include that into this version due to time constraints.

## Tech stack
- React
- Redux
- Material UI (React)
- Gitlab CI/CD

## Local Development

Run locally:
```
yarn start
```

Running tests:
```
yarn test
```

