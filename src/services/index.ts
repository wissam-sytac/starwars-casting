import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/dist/query/react';
import {Film, PaginateddResponse, Person} from '../app/types';
import {baseUrl} from '../app/config';

export const swApi = createApi({
  reducerPath: 'swApi',
  baseQuery: fetchBaseQuery({ baseUrl }),
  endpoints: (builder) => ({
    getPeople: builder.query<PaginateddResponse<Person>, string | void>({
      query: (pageNumber) => pageNumber ? `people/?page=${pageNumber}` : `people/`,
    }),
    getPersonById: builder.query<Person, string | void>({
      query: (filmId) => `people/${filmId}`,
    }),
    getAllFilms: builder.query<PaginateddResponse<Film>, string | void>({
      query: () => 'films/'
    }),
    getFilmById: builder.query<Film, string | void>({
      query: (filmId) => `films/${filmId}`,
    }),
  }),
});

export const {
  useGetPeopleQuery,
  useGetPersonByIdQuery,
  useGetAllFilmsQuery,
  useGetFilmByIdQuery,
  endpoints,
} = swApi;
