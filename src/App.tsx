import React from 'react';
import {Page} from './features/casting/components/Page';
import { Routes, Route, HashRouter } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <HashRouter>
        <Routes>
          <Route path="/" element={<Page />} />
          <Route path="/people/:personId" element={<Page />} />
        </Routes>
      </HashRouter>
    </div>
  );
}

export default App;