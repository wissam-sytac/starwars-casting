import {Box, Button, Checkbox, CircularProgress, Divider, FormControlLabel, Typography} from '@mui/material';
import {useEffect, useState} from 'react';
import {Film, Filters} from '../../../app/types';

type SearchFiltersProps = {
  isLoading: boolean;
  hasError: boolean;
  error?: string;
  films: Film[];
  handleApply: Function;
};

const initialState: Filters = {
  films: [],
};

export function SearchFilters(props: SearchFiltersProps) {
  const { handleApply } = props;
  const [ filters, setFilters ] = useState(initialState);

  useEffect(() => {
    handleApply(filters);
    // eslint-disable-next-line
  }, [filters]);

  return (
    <div>
      <Typography variant="h5" component="h5">
        Filters
      </Typography>

      <Box marginBottom={1}>
        <Box marginTop={1} marginBottom={1}>
          <Typography variant="h6" component="h6">
            Films
          </Typography>

          {props.isLoading && <CircularProgress />}

          {!props.isLoading && !props.hasError && props.films.map(film => (
            <FormControlLabel
              key={film.url}
              label={film.title}
              control={
                <Checkbox
                  name={film.url}
                  checked={filters.films.includes(film.url)}
                  onChange={(evt) => {
                    const { url } = film;
                    if (filters.films.includes(url)) {
                      setFilters({
                        ...filters,
                        films: filters.films.filter(f => f !== url),
                      });
                      return;
                    }
                    setFilters({
                      ...filters,
                      films: [
                        ...filters.films,
                        film.url,
                      ],
                    });
                  }}
                />
              }
            />
          ))}
        </Box>
        <Divider />
      </Box>

      <Button onClick={() => { setFilters({ films: [] }); }} variant="outlined">reset</Button>
    </div>
  );
}
