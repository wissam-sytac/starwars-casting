import { useAppSelector, useAppDispatch } from '../../../app/hooks';
import styles from './Page.module.css';
import {SearchFilters} from './SearchFilters';
import {Results, ResultsProps} from './Results';
import {
  AppBar,
  Grid,
  Toolbar,
  Typography,
  Card,
  CardContent,
  Drawer,
  Box,
  List,
  ListItem,
  Chip,
  CircularProgress
} from '@mui/material';
import {Pagination} from './Pagination';
import {applyFilters, selectFilters, selectFiltersLoading, selectPeople, personWithFilms, selectPerson} from '../slice';
import {useGetAllFilmsQuery, useGetPeopleQuery} from '../../../services';
import {useEffect, useState} from 'react';
import {areAnyFiltersSet, personIdFromPersonUri} from '../utils';
import {Film, Filters, Person} from '../../../app/types';

// @TODO Utilize Layout.tsx to remove markup from here

export function Page() {
  const dispatch = useAppDispatch();
  const { data: filmsData, isFetching: isFetchingMovies } = useGetAllFilmsQuery();
  const [page, setPage] = useState(1);
  const [drawerOpen, setDrawerOpen] = useState(false);
  const [selectedPerson, setSelectedPerson] = useState<Person | undefined>(undefined);
  const people = useAppSelector(selectPeople);
  const filtersLoading = useAppSelector(selectFiltersLoading);
  const filters = useAppSelector(selectFilters);
  const selectedPersonFromStore = useAppSelector(selectPerson);

  // Only call this hook if no filters are selected
  const { data, isFetching, isError } = useGetPeopleQuery(page?.toString(), { skip: areAnyFiltersSet(filters) });

  const handleFilterChange = (x: Filters) => {
    dispatch(applyFilters(x));
    setPage(1);
  };

  const toggleDrawer = (val: boolean) => () => {
    setDrawerOpen(val);
    setSelectedPerson(undefined);
  };

  const handleClickDetails = (person: Person) => {
    setDrawerOpen(true);
    setSelectedPerson(person);
  };

  useEffect(() => {
    if (selectedPerson) {
      dispatch(personWithFilms(personIdFromPersonUri(selectedPerson.url)));
    }
    // eslint-disable-next-line
  }, [selectedPerson]);

  // @TODO: refactor this
  // Used to split logic between filtered and non filtered results
  const createData = (): ResultsProps => {
    if (!areAnyFiltersSet(filters)) {
      return {
        isLoading: isFetching,
        hasError: isError,
        people: data?.results || [],
        handleClickDetails,
      };
    }
    if (filtersLoading) {
      return {
        isLoading: true,
        hasError: false,
        people: [],
        handleClickDetails,
      };
    }

    return {
      isLoading: false,
      hasError: false,
      // @ts-ignore
      people: people.filter(person => !!person),
      handleClickDetails,
    };
  };

  const renderPagination = () => {
    if (areAnyFiltersSet(filters)) {
      return null;
    }
    return (
      <Pagination
        previous={data?.previous}
        next={data?.next}
        handleClickPrevious={() => setPage(page - 1)}
        handleClickNext={() => setPage(page + 1)}
      />
    );
  };

  const createSelectedPersonFilmsData = (): Film[] => {
    if (selectedPersonFromStore && selectedPersonFromStore.films) {
      // @ts-ignore
      return selectedPersonFromStore.films.filter(f => !!f) || [];
    }
    return [];
  };

  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <AppBar position="static" className={styles.appBar}>
          <Toolbar>
            <Typography variant="h5" component="div" sx={{ flexGrow: 1 }}>
              StarWars Casting
            </Typography>
          </Toolbar>
        </AppBar>
        <Grid container spacing={1}>
          <Grid item xs={12} md={4} lg={3}>
            <Card>
              <CardContent>
                <SearchFilters
                  isLoading={isFetchingMovies}
                  hasError={false}
                  films={filmsData?.results || []}
                  handleApply={handleFilterChange}
                />
              </CardContent>
            </Card>
          </Grid>

          <Grid item xs={12} md={8} lg={9}>
            <Card>
              {renderPagination()}
              <Results {...createData()}/>
              {renderPagination()}
            </Card>
          </Grid>
        </Grid>
      </div>

      <Drawer
        anchor={'right'}
        open={drawerOpen}
        onClose={toggleDrawer(false)}
      >
        <Box
          sx={{ width: 250 }}
          role="presentation"
          onClick={toggleDrawer(false)}
          onKeyDown={toggleDrawer(false)}
        >
          <List>
            <ListItem>
              <Typography variant="h5" component="div" sx={{ flexGrow: 1 }}>
                {selectedPerson?.name}
              </Typography>
            </ListItem>
            <ListItem>
              <Typography variant="body1" sx={{ flexGrow: 1 }}>
                Gender: {selectedPerson?.gender}
              </Typography>
            </ListItem>
            <ListItem>
              <Typography variant="body1" sx={{ flexGrow: 1 }}>
                Born: {selectedPerson?.birth_year}
              </Typography>
            </ListItem>
            <ListItem>
              <Typography variant="body1" sx={{ flexGrow: 1 }}>
                Movies
              </Typography>
            </ListItem>
            {selectedPersonFromStore.isLoading && <CircularProgress />}
            {createSelectedPersonFilmsData().map((film) => <ListItem key={film.url}><Chip label={film.title} /></ListItem>)}
          </List>
        </Box>
      </Drawer>
    </div>
  );
}
