import React, {Fragment} from 'react';
import {ListItem, ListItemAvatar, Avatar, ListItemText, Typography, Button, Divider} from '@mui/material';
import {Person} from '../../../app/types';

type ResultProps = {
  person: Person;
  handleClickDetails: Function;
};

export function Result(props: ResultProps) {
  const {person, handleClickDetails} = props;

  return (
    <Fragment>
      <ListItem alignItems="flex-start" secondaryAction={<Button variant={'contained'} onClick={() => handleClickDetails(person)}>details</Button>}>
        <ListItemAvatar>
          <Avatar alt={person.name} />
        </ListItemAvatar>

        <ListItemText
          primary={person.name}
          secondary={
            <Fragment>
              <Typography
                sx={{ display: 'inline' }}
                component="span"
                variant="body2"
                color="text.primary"
              >
                Born: {person.birth_year}
              </Typography>
              - Gender: {person.gender}
            </Fragment>
          }
        />
      </ListItem>
      <Divider variant="inset" component="li" />
    </Fragment>
  );
}
