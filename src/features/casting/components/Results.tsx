import React, {Fragment} from 'react';
import {CircularProgress, List} from '@mui/material';
import {Result} from './Result';
import {Person} from '../../../app/types';

export type ResultsProps = {
  isLoading: boolean;
  hasError: boolean;
  error?: string;
  people: Person[];
  handleClickDetails: Function;
};

export function Results(props: ResultsProps) {
  const {isLoading, hasError, people, error, handleClickDetails} = props;

  if (isLoading) {
    return <CircularProgress />
  }

  if (hasError) {
    return <div>{error}</div>
  }

  return (
    <Fragment>
      <List>
        {people.map(person => (
          <Result key={person.url} person={person} handleClickDetails={handleClickDetails} />
        ))}
      </List>
    </Fragment>
  );
}