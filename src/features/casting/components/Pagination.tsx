import {Button} from '@mui/material';

type PaginationProps = {
  previous?: string;
  next?: string;
  handleClickPrevious: Function;
  handleClickNext: Function;
}

export function Pagination(props: PaginationProps) {
  return (
    <div>
      {props.previous && <Button onClick={() => props.handleClickPrevious()}>Previous</Button>}
      {props.next && <Button onClick={() => props.handleClickNext()}>Next</Button>}
    </div>
  );
}
