import styles from './Layout.module.css';
import {AppBar, Card, CardContent, Grid, Toolbar, Typography} from '@mui/material';

export function Layout() {
  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <AppBar position="static" className={styles.appBar}>
          <Toolbar>
            <Typography variant="h5" component="div" sx={{ flexGrow: 1 }}>
              StarWars Casting
            </Typography>
          </Toolbar>
        </AppBar>
        <Grid container spacing={1}>
          <Grid item xs={12} md={4}>
            <Card>
              <CardContent>
                props.SearchFilter
              </CardContent>
            </Card>
          </Grid>

          <Grid item xs={12} md={8}>
            <Card>
              props.pagination
              props.results
              props.pagination
            </Card>
          </Grid>
        </Grid>
      </div>
    </div>
  );
}
