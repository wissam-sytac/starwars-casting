import castingReducer, {
  setFilters,
  resetFilters,
  FiltersState,
} from './slice';

describe('Filters reducer', () => {
  const initialState: FiltersState = {
    filters: {
      films: [],
    },
    people: [],
    isLoading: false,
    selectedPerson: {
      isLoading: false,
      films: [],
    }
  };

  it('should handle initial state', () => {
    expect(castingReducer(undefined, { type: undefined })).toEqual({
      filters: {
        films: [],
      },
      people: [],
      isLoading: false,
      selectedPerson: {
        isLoading: false,
        films: [],
      }
    });
  });

  it('should handle setFilters', () => {
    const actual = castingReducer(initialState, setFilters({ films: ['film1', 'film2'] }));
    expect(actual.filters).toEqual({
      films: ['film1', 'film2'],
    });
  });

  it('should handle resetFilters', () => {
    const actual = castingReducer({
      filters: {
        films: [
          'filmA',
          'filmB',
        ],
      },
      people: [],
      isLoading: false,
      selectedPerson: {
        isLoading: false,
        films: [],
      }
    }, resetFilters());
    expect(actual.filters).toEqual({
      films: [],
    });
  });
});
