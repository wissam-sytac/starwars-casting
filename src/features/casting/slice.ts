import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import {RootState} from '../../app/store';
import {Film, Filters, Person} from '../../app/types';
import {filmIdFromFilmUri, intersectCharacters, personIdFromPersonUri} from './utils';
import {endpoints} from '../../services';

export interface FiltersState {
  filters: Filters;
  people: (Person | undefined)[];
  isLoading: boolean;
  selectedPerson: {
    isLoading: boolean;
    person?: Person;
    films?: (Film | undefined)[];
  },
}

const initialState: FiltersState = {
  filters: {
    films: [],
  },
  people: [],
  isLoading: false,
  selectedPerson: {
    isLoading: false,
    films: [],
  },
}

export const applyFilters = createAsyncThunk(
  'sw/filters',
  async (filters: Filters, { dispatch }) => {
    dispatch(castingSlice.actions.setFilters(filters));
    const {films} = filters;
    const selectedFilms = await Promise.all(
      films.map(filmIdFromFilmUri).map(film => (
        dispatch(endpoints.getFilmById.initiate(film))
      ))
    );
    const fs: (Film | undefined)[] = selectedFilms.filter(f => f).map(f => f.data);
    const peopleSets = fs.map(f => f?.characters);

    // @ts-ignore
    const peopleNoDupes: string[] = intersectCharacters(peopleSets.filter(set => !!set));

    const peopleResponses = await Promise.all(
      peopleNoDupes.map(pid => (
        dispatch(endpoints.getPersonById.initiate(personIdFromPersonUri(pid)))
      ))
    );

    const resPeople: (Person | undefined)[] = peopleResponses.filter(p => p).map(f => f.data);

    return resPeople;
  }
);

export const personWithFilms = createAsyncThunk(
  'sw/personWithFilms',
  async (personId: string, { dispatch }) => {
    const personResponse = await dispatch(endpoints.getPersonById.initiate(personId));
    if (personResponse.data) {
      const selectedFilms = await Promise.all(
        personResponse.data.films.map(filmIdFromFilmUri).map(film => (
          dispatch(endpoints.getFilmById.initiate(filmIdFromFilmUri(film)))
        ))
      );
      const fs: (Film | undefined)[] = selectedFilms.filter(f => f).map(f => f.data);
      return {
        person: personResponse.data,
        films: fs,
      };
    }
    return {
      person: undefined,
      films: [],
    };
  }
);

export const castingSlice = createSlice({
  name: 'casting',
  initialState,
  reducers: {
    setFilters: (state, action: PayloadAction<Filters>) => {
      state.filters = action.payload;
    },
    resetFilters: (state) => {
      state.filters = initialState.filters;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(applyFilters.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(applyFilters.fulfilled, (state, action) => {
        state.isLoading = false;
        state.people = action.payload;
      })
      .addCase(personWithFilms.pending, (state) => {
        state.selectedPerson = {
          isLoading: true,
          films: [],
        };
      })
      .addCase(personWithFilms.fulfilled, (state, action) => {
        state.selectedPerson = {
          isLoading: false,
          person: action.payload?.person,
          films: action.payload ? action.payload.films : [],
        };
      })
  },
});

export const selectPeople = (state: RootState) => state.filters.people;

export const selectFilters = (state: RootState) => state.filters.filters;

export const selectFiltersLoading = (state: RootState) => state.filters.isLoading;

export const selectPerson = (state: RootState) => state.filters.selectedPerson;

export const { setFilters, resetFilters } = castingSlice.actions;

export default castingSlice.reducer;
