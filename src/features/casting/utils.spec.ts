import {intersectCharacters, personIdFromPersonUri, filmIdFromFilmUri} from './utils';

describe('intersectCharacters', () => {
  it('should work correctly for 1 input', () => {
    const res = intersectCharacters([
      ['a', 'b', 'c'],
    ]);
    expect(res).toEqual(['a', 'b', 'c']);
  });

  it('should work correctly for 2 inputs', () => {
    const res = intersectCharacters([
      ['a', 'b', 'c'],
      ['a', 'b', 'd'],
    ]);
    expect(res).toEqual(['a', 'b']);
  });

  it('should work correctly for 3 inputs', () => {
    const res = intersectCharacters([
      ['a', 'b', 'c'],
      ['a', 'b', 'd', 'e'],
      ['b', 'a'],
    ]);
    expect(res).toEqual(['a', 'b']);
  });
});

describe('personIdFromPersonUri', () => {
  it('should work correctly', () => {
    const res = personIdFromPersonUri('https://swapi.dev/api/people/1/');
    expect(res).toEqual('1');
  });
});

describe('filmIdFromFilmUri', () => {
  it('should work correctly', () => {
    const res = filmIdFromFilmUri('https://swapi.dev/api/films/9/');
    expect(res).toEqual('9');
  });
});
