import {Filters} from '../../app/types';
import {baseUrl} from '../../app/config';

export const personIdFromPersonUri = (str: string): string => str.replace(`${baseUrl}people/`, '').replace('/', '');

export const filmIdFromFilmUri = (str: string): string => str.replace(`${baseUrl}films/`, '').replace('/', '');

export function intersectCharacters(sets: string[][]): string[] {
  if (!sets.length) {
    return [];
  }
  if (sets.length === 1) {
    return sets[0];
  }
  return sets.reduce((acc, set: string[]) => {
    return acc.filter(i => set.includes(i));
  }, sets[0]);
}

export const areAnyFiltersSet = (filters: Filters): boolean => !!filters.films.length;
