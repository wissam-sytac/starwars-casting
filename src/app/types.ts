export interface Person {
  url: string;
  name: string;
  height: string;
  films: string[];
  birth_year: string;
  gender: string;
}

export interface Film {
  url: string;
  title: string;
  characters: string[];
}

export interface PaginateddResponse<Entity> {
  count: number;
  results: Entity[];
  previous: string;
  next: string;
}

export interface Filters {
  films: string[];
}
