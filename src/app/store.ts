import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import {swApi} from '../services';
import castingReducer from '../features/casting/slice';

export const store = configureStore({
  reducer: {
    [swApi.reducerPath]: swApi.reducer,
    filters: castingReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(swApi.middleware),
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
